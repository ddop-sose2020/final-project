<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <groupId>de.hallinto.core</groupId>
    <artifactId>hallinto-core</artifactId>
    <version>1.0</version>
    <packaging>war</packaging>

    <dependencies>
        <!-- junit -->
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>${junit-version}</version>
            <scope>test</scope>
        </dependency>

        <!-- javaee -->
        <dependency>
            <groupId>javax</groupId>
            <artifactId>javaee-api</artifactId>
            <version>${javaee-version}</version>
            <scope>provided</scope>
        </dependency>

        <!-- jaxrs-unit -->
        <dependency>
            <groupId>io.github.binout</groupId>
            <artifactId>jaxrs-unit-api</artifactId>
            <version>1.1.1</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>io.github.binout</groupId>
            <artifactId>jaxrs-unit-resteasy</artifactId>
            <version>1.1.1</version>
            <scope>test</scope>
        </dependency>

        <!-- swagger -->
        <dependency>
            <groupId>io.swagger</groupId>
            <artifactId>swagger-jaxrs</artifactId>
            <version>${swagger-version}</version>
            <scope>compile</scope>
        </dependency>

        <!-- swagger annotations -->
        <dependency>
            <groupId>io.swagger</groupId>
            <artifactId>swagger-annotations</artifactId>
            <version>${swagger-version}</version>
        </dependency>

        <!-- mongodb -->
        <dependency>
            <groupId>org.hibernate.ogm</groupId>
            <artifactId>hibernate-ogm-mongodb</artifactId>
            <version>${hibernate-version}</version>
        </dependency>

        <!-- hibernate in-mem-database -->
        <dependency>
            <groupId>com.h2database</groupId>
            <artifactId>h2</artifactId>
            <version>1.4.197</version>
            <scope>test</scope>
        </dependency>

        <!-- jackson -->
        <dependency>
            <groupId>com.fasterxml.jackson.jaxrs</groupId>
            <artifactId>jackson-jaxrs-json-provider</artifactId>
            <version>${jackson-version}</version>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.datatype</groupId>
            <artifactId>jackson-datatype-jdk8</artifactId>
            <version>${jackson-version}</version>
        </dependency>

        <!-- Bean Validation API support -->
        <!--
        <dependency>
            <groupId>javax.validation</groupId>
            <artifactId>validation-api</artifactId>
            <version>1.1.0.Final</version>
            <scope>provided</scope>
        </dependency>
        -->
        <!-- https://mvnrepository.com/artifact/org.apache.cxf/cxf-rt-frontend-jaxrs -->
        <!-- Version is just a guess -->
        <!-- IBM WebSphere Application Server Liberty Core v8.5.5.6 uses v3.0.2, source https://www.ibm.com/support/knowledgecenter/SSD28V_8.5.5/com.ibm.websphere.wlp.core.doc/ae/rwlp_jaxrs_secure.html -->
        <!--
        <dependency>
            <groupId>org.apache.cxf</groupId>
            <artifactId>cxf-rt-frontend-jaxrs</artifactId>

            <version>3.0.2</version>
            <scope>provided</scope>
        </dependency>
        -->
    </dependencies>

    <build>
        <!-- Main source directory -->
        <sourceDirectory>src/main/java</sourceDirectory>

        <finalName>${project.artifactId}-${project.version}</finalName>
        <plugins>
            <plugin>
                <groupId>com.googlecode.maven-download-plugin</groupId>
                <artifactId>download-maven-plugin</artifactId>
                <version>1.6.0</version>
                <executions>
                    <execution>
                        <id>swagger-ui</id>
                        <phase>prepare-package</phase>
                        <goals>
                            <goal>wget</goal>
                        </goals>
                        <configuration>
                            <url>https://github.com/swagger-api/swagger-ui/archive/master.tar.gz</url>
                            <unpack>true</unpack>
                            <outputDirectory>${project.build.directory}</outputDirectory>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <artifactId>maven-resources-plugin</artifactId>
                <version>2.6</version>
                <executions>
                    <execution>
                        <id>copy-resources</id>
                        <phase>prepare-package</phase>
                        <goals>
                            <goal>copy-resources</goal>
                        </goals>
                        <configuration>
                            <outputDirectory>target/${project.artifactId}-${project.version}</outputDirectory>
                            <resources>
                                <resource>
                                    <directory>${project.build.directory}/swagger-ui-master/dist</directory>
                                    <filtering>true</filtering>
                                    <excludes>
                                        <exclude>index.html</exclude>
                                    </excludes>
                                </resource>
                            </resources>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

            <!-- Add src/gen/java to compilation -->
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>build-helper-maven-plugin</artifactId>
                <version>1.9.1</version>
                <executions>
                    <execution>
                        <id>add-source</id>
                        <phase>generate-sources</phase>
                        <goals>
                            <goal>add-source</goal>
                        </goals>
                        <configuration>
                            <sources>
                                <source>src/gen/java</source>
                            </sources>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>
    <properties>
        <!--sonarcloud-->
        <sonar.projectKey>ddop-sose2020_final-project</sonar.projectKey>
        <sonar.organization>ddop-sose2020-sonarcloud</sonar.organization>

        <resteasy-version>4.0.0.Beta2</resteasy-version>

        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
        <failOnMissingWebXml>false</failOnMissingWebXml>

        <junit-version>4.12</junit-version>
        <javaee-version>8.0</javaee-version>
        <swagger-version>1.5.18</swagger-version>
        <mongodb-version>3.6.3</mongodb-version>
        <jackson-version>2.9.5</jackson-version>

        <hibernate-version>5.3.0.Final</hibernate-version>
    </properties>
</project>