package de.hallinto.core.rest;

import de.hallinto.core.model.dao.RolleDAO;
import de.hallinto.core.model.entity.Rolle;
import de.hallinto.core.rest.api.RollenApiService;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@RequestScoped
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaJAXRSCXFCDIServerCodegen", date = "2018-04-26T21:20:51.095+02:00")
public class RollenApiServiceImpl implements RollenApiService {

    @Inject
    RolleDAO rolleDAO;

    /**
     * @param rolle           Rolle die gespeichert werden soll
     * @param securityContext
     * @return
     */
    @Override
    public Response addRolle(Rolle rolle, SecurityContext securityContext) {
        rolleDAO.persist(rolle);
        return Response.ok().entity(rolle).build();
    }

    /**
     * @param id              ID der Rolle
     * @param securityContext
     * @return
     */
    @Override
    public Response deleteRolle(String id, SecurityContext securityContext) {
        rolleDAO.remove(rolleDAO.findById(id));
        return Response.ok().build();
    }

    /**
     * @param id              ID der Rolle
     * @param securityContext
     * @return
     */
    @Override
    public Response findRolleById(String id, SecurityContext securityContext) {
        return Response.ok().entity(rolleDAO.findById(id)).build();
    }

    /**
     * @param limit           Anzahl auf die die Ergebnisse limitiert werden sollen
     * @param securityContext
     * @return
     */
    @Override
    public Response findRollen(Integer limit, SecurityContext securityContext) {
        return Response.ok().entity(rolleDAO.findAll(limit)).build();
    }
}
