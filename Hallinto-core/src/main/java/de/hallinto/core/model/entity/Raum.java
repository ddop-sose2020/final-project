package de.hallinto.core.model.entity;

import javax.persistence.Entity;

@Entity
public class Raum extends HEntity {
    private String bezeichnung;
    private int nummer;

    public String getBezeichnung() {
        return bezeichnung;
    }

    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }

    public int getNummer() {
        return nummer;
    }

    public void setNummer(int nummer) {
        this.nummer = nummer;
    }
}
