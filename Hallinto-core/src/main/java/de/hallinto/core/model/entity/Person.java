package de.hallinto.core.model.entity;

import javax.persistence.MappedSuperclass;
import java.util.Date;

@MappedSuperclass
public abstract class Person extends HEntity {

    protected String vorname;
    protected String nachname;
    protected String geschlecht;
    protected Date geburtsdatum;
    protected String kostentraeger;

    /**
     * @return Vorname der Person
     */
    public String getVorname() {
        return vorname;
    }

    /**
     * @param vorname
     */
    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    /**
     * @return Nachname der Person
     */
    public String getNachname() {
        return nachname;
    }

    /**
     * @param nachname
     */
    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    /**
     * @return Geschlecht der Person
     */
    public String getGeschlecht() {
        return geschlecht;
    }

    /**
     * @param geschlecht
     */
    public void setGeschlecht(String geschlecht) {
        this.geschlecht = geschlecht;
    }

    /**
     * @return Geburtsdatum der Person
     */
    public Date getGeburtsdatum() {
        return geburtsdatum;
    }

    /**
     * @param geburtsdatum
     */
    public void setGeburtsdatum(Date geburtsdatum) {
        this.geburtsdatum = geburtsdatum;
    }

    /**
     * @return Kostenträger der Person
     */
    public String getKostentraeger() {
        return kostentraeger;
    }

    /**
     * @param kostentraeger
     */
    public void setKostentraeger(String kostentraeger) {
        this.kostentraeger = kostentraeger;
    }
}
