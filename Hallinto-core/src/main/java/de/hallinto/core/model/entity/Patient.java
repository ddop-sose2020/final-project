package de.hallinto.core.model.entity;

import javax.persistence.Entity;

@Entity
public class Patient extends Person {
    private String krankenkasse;
    private String versicherungsnummer;
    private String versicherungsstatus;

    /**
     * @return Krankenkasse der Person
     */
    public String getKrankenkasse() {
        return krankenkasse;
    }

    /**
     * @param krankenkasse
     */
    public void setKrankenkasse(String krankenkasse) {
        this.krankenkasse = krankenkasse;
    }

    /**
     * @return Versicherungsnummer des Patienten
     */
    public String getVersicherungsnummer() {
        return versicherungsnummer;
    }

    /**
     * @param versicherungsnummer
     */
    public void setVersicherungsnummer(String versicherungsnummer) {
        this.versicherungsnummer = versicherungsnummer;
    }

    /**
     * @return Versicherungsstatus des Patienten
     */
    public String getVersicherungsstatus() {
        return versicherungsstatus;
    }

    /**
     * @param versicherungsstatus
     */
    public void setVersicherungsstatus(String versicherungsstatus) {
        this.versicherungsstatus = versicherungsstatus;
    }
}
