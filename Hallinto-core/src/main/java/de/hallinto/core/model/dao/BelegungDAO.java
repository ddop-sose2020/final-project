package de.hallinto.core.model.dao;

import de.hallinto.core.model.entity.Belegung;
import de.hallinto.core.model.entity.Raum;

import javax.inject.Inject;
import javax.persistence.TypedQuery;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Data Access Object (DAO) zur vereinfachten Handhabung der Entity Belegung.
 *
 * @see Belegung
 * @see de.hallinto.core.model.dao.GenericDAO
 */
public class BelegungDAO extends GenericDAO<Belegung> {

    @Inject
    RaumDAO daoRaum;

    /**
     * Diese Funktion formatiert ein übergebenes Date Objekt {@param date} in einen String nach dem ISO8601 Standard.
     *
     * @param date Datum welches formatiert werden soll
     * @return Das übergebene Datum als ISO8601 String
     */
    private static String toISO8601UTC(Date date) {
        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'");
        df.setTimeZone(tz);
        return df.format(date);
    }

    public List<Belegung> getByRaumIdAndStartEnd(String sRaumId, long start, long end) {

        Raum raum = daoRaum.findById(sRaumId);
/*
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();

        CriteriaQuery<Belegung> q = cb.createQuery(Belegung.class);
        Root<Belegung> belegung = q.from(Belegung.class);
        ParameterExpression<Raum> r = cb.parameter(Raum.class);
        q.select(belegung).where(cb.equal(belegung.get("raum"), r));*/

  /*      TypedQuery<Belegung> query = entityManager.createQuery(
                "SELECT b FROM " + clsEntity.getSimpleName() + " AS b WHERE b.raum.id = :raum", Belegung.class);

        List<Belegung> belegungen =  query.setParameter("raum", sRaumId).getResultList();*/

/*
      TypedQuery<Belegung> query = entityManager.createQuery(q);
      query.setParameter(r, raum);

      List<Belegung> belegungen = query.getResultList();
*/
/*
        // Native query, weil Hibernate wohl keine OneToOne relationen unterstützt ...
        String query1 = "{ $and: [" +
                    "{'raum_id': ObjectId('" + sRaumId + "')}," +
                    "{'datum': {" +
                        "'$gte': " + start + "," +
                        "'$lt': " + end + "" +
                        "}" +
                    "}" +
                "]}";

        List<Belegung> belegungen = entityManager.createNativeQuery( query1, Belegung.class ).getResultList();

        */

        // Leider manuell ... mongodb + hibernate = experimental ... -.-
        LinkedList<Belegung> belegungen = new LinkedList<>();
        List<Belegung> allBelegungen = this.findAll();

        allBelegungen.forEach(belegung -> {
            if (belegung.getRaum() != null && belegung.getRaum().getId().equals(sRaumId)
                    && belegung.getSollUhrzeitVon() != null && belegung.getSollUhrzeitVon().after(new Date(start * 1000))
                    && belegung.getSollUhrzeitBis() != null && belegung.getSollUhrzeitBis().before(new Date(end * 1000))) {
                belegungen.push(belegung);
            }
        });

        return belegungen;
    }

    public Belegung getByRaumIdAndCurrentAktive(String sRaumId) {
        LinkedList<Belegung> belegungen = new LinkedList<>();
        List<Belegung> allBelegungen = this.findAll();

        allBelegungen.forEach(belegung -> {
            if (belegung.getRaum() != null && belegung.getRaum().getId().equals(sRaumId)
                    && belegung.getCurrentAktive() == true) {
                belegungen.push(belegung);
            }
        });

        if(belegungen.size() > 1) {
            throw new Error("getByRaumIdAndCurrentAktive :: more than one current active in room");
        }
        if(belegungen.size() == 0)
            return null;


        return belegungen.get(0);
    }

    public Belegung getByToken(String sToken) {
        TypedQuery<Belegung> query = entityManager.createQuery(
                "SELECT b FROM " + clsEntity.getSimpleName() + " AS b WHERE b.token = :token", Belegung.class);

        Belegung belegung =  query.setParameter("token", sToken).getSingleResult();

        return belegung;
    }

    public List<Belegung> getByPatId(String sPatId) {
        LinkedList<Belegung> belegungen = new LinkedList<>();
        List<Belegung> allBelegungen = this.findAll();

        allBelegungen.forEach(belegung -> {
            if (belegung.getPat() != null && belegung.getPat().getId().equals(sPatId)) {
                belegungen.push(belegung);
            }
        });

        return belegungen;
    }

    public List<Belegung> getByRaumId(String sRaumId) {
        LinkedList<Belegung> belegungen = new LinkedList<>();
        List<Belegung> allBelegungen = this.findAll();

        allBelegungen.forEach(belegung -> {
            if (belegung.getRaum() != null && belegung.getRaum().getId().equals(sRaumId)) {
                belegungen.push(belegung);
            }
        });

        return belegungen;
    }
}

