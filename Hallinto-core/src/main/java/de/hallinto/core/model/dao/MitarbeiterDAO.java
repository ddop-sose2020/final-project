package de.hallinto.core.model.dao;

import de.hallinto.core.model.entity.Mitarbeiter;
import de.hallinto.core.model.entity.Rolle;

import javax.inject.Inject;
import javax.persistence.TypedQuery;
import java.security.InvalidParameterException;


/**
 * Data Access Object (DAO) zur vereinfachten Handhabung der Entity Mitarbeiter.
 *
 * @see Mitarbeiter
 * @see de.hallinto.core.model.dao.GenericDAO
 */
public class MitarbeiterDAO extends GenericDAO<Mitarbeiter> {

    @Inject
    RolleDAO rolleDAO;

    /**
     * @param mitarbeiter Mitarbeiter der mit einer Rolle versehen werden soll
     * @param rolle       Rolle die einem Mitarbeiter zugeordnert werden soll
     * @return True wenn die Rolle dem Mitarbeiter erfolgreich zugeordnet werden konnte, False falls der diese Rolle schon besitzt.
     * @throws InvalidParameterException Falls eine Übergebene Entity nicht in der Datenbank vorhanden ist.
     */
    public boolean addRolle(Mitarbeiter mitarbeiter, Rolle rolle) throws InvalidParameterException {
        if (findById(mitarbeiter.getId()) != null) {

            if (findById(rolle.getId()) == null) {

                // Suche in bereits zugewiesenen Rollen nach der neuen Rolle
                boolean hasRolle = false;
                for (Rolle bereitsZugewiesen : mitarbeiter.rollen) {
                    hasRolle = bereitsZugewiesen.getId().equals(rolle.getId()) || hasRolle;
                }

                if (hasRolle) {

                    // Rolle wurde bereits zugewiesen
                    return false;
                } else {

                    // Füge neue Rolle hinzu falls noch nicht zugewiesen
                    mitarbeiter.rollen.add(rolle);
                    this.persist(mitarbeiter);

                    // Rolle wurde erfolgreich zugewiesen
                    return true;
                }
            } else throw new InvalidParameterException("Die Rolle mit der id=" + rolle.getId() + " existiert nicht!");
        } else
            throw new InvalidParameterException("Der Mitarbeiter mit der id=" + mitarbeiter.getId() + " existiert nicht!");
    }

    public Mitarbeiter findByUsername(String username) {
        TypedQuery<Mitarbeiter> query = entityManager.createQuery(
                "SELECT m FROM " + clsEntity.getSimpleName() + " AS m WHERE m.username = :username", Mitarbeiter.class);
        Mitarbeiter mitarbeiter = query.setParameter("username", username).getSingleResult();

        return mitarbeiter;
    }
}
