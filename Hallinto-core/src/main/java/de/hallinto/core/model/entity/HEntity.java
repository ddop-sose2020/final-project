package de.hallinto.core.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.io.Serializable;

@MappedSuperclass
public abstract class HEntity implements Serializable {

    public static final String ID = "id";
    public static final String DEFVAL = "defval";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Type(type = "objectid")
    @Column(name = ID)
    protected String fldId;

    /**
     * @return
     */
    public String getId() {
        return fldId;
    }

    /**
     * @param sId
     */
    public void setId(String sId) {
        fldId = sId;
    }

    /**
     * @return
     */
    @Null
    @JsonIgnore
    public Object getDefaultValue() {
        return null;
    }

    /**
     * @return
     */
    @NotNull
    @Override
    public String toString() {
        Object objDefaultValue = getDefaultValue();
        return objDefaultValue == null ? String.valueOf(getId()) : objDefaultValue.toString();
    }

    /**
     * @return
     */
    @Override
    public int hashCode() {
        return getId().hashCode();
    }

    /**
     * @param objOther Objekt mit dem verglichen werden soll
     * @return
     */
    @Override
    public boolean equals(Object objOther) {
        if (this == objOther)
            return true;
        if (objOther == null)
            return false;
        if (!(objOther instanceof HEntity))
            return false;

        return getId().equals(((HEntity) objOther).getId());
    }
}
