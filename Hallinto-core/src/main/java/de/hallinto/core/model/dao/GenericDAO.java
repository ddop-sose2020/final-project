package de.hallinto.core.model.dao;

import de.hallinto.core.model.entity.HEntity;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.transaction.UserTransaction;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @param <E>
 */
public abstract class GenericDAO<E extends HEntity> {

    private static Map<Class<?>, Class<?>> MAP_DAO_CLSENTITY = new ConcurrentHashMap<>();

    @PersistenceContext
    protected EntityManager entityManager;
    protected Class<E> clsEntity;
    @Resource
    private UserTransaction userTransaction;

    /**
     * Initialisiert
     */
    @PostConstruct
    private void init() {
        clsEntity = (Class<E>) MAP_DAO_CLSENTITY.get(this.getClass());
        if (clsEntity == null) {
            Class<?> clsDAO = getClass();

            Type type = null;
            do {
                type = clsDAO.getGenericSuperclass();
                if (!(type instanceof ParameterizedType))
                    clsDAO = (Class<?>) type;
            } while (!(type instanceof ParameterizedType));

            ParameterizedType typeParam = (ParameterizedType) clsDAO.getGenericSuperclass();
            clsEntity = (Class<E>) typeParam.getActualTypeArguments()[0];

            MAP_DAO_CLSENTITY.put(this.getClass(), clsEntity);
        }
    }

    /**
     * Speichere eine Entity in die Datenbank.
     *
     * @param entity Entity die gespeichert werden soll.
     * @return Aktualisierte Entity
     */
    @Transactional
    public E persist(E entity) {
        E resultEntity = null;

        try {
            // userTransaction.begin();


            if (entity.getId() == null || entity.getId().equals(""))
                entityManager.persist(entity);

            resultEntity = entityManager.merge(entity);

            //userTransaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            try {
                //userTransaction.rollback();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return resultEntity;
    }

    /**
     * Löscht eine Entity auf Basis ihrer Signatur.
     *
     * @param entity Entity die gelöscht werden soll
     */
    @Transactional
    public void remove(E entity) {
        try {
            //userTransaction.begin();

            // If entity is not in current transaction, add (=merge) it to current transaction !
            entity = entityManager.contains(entity) ? entity : entityManager.merge(entity);

            entityManager.remove(entity);

            //userTransaction.commit();

        } catch (Exception e) {
            e.printStackTrace();
            try {
                //  userTransaction.rollback();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    /**
     * Gibt eine Entity auf Basis ihrer ID.
     *
     * @param sId Entity-ID die gefunden werden soll
     */
    public E findById(String sId) {
        E entity = entityManager.find(clsEntity, sId);
        return entity;
    }

    /**
     * Löscht eine Entity auf Basis ihrer ID.
     *
     * @param sId Die ID einer Entity
     * @see HEntity#ID
     */
    @Transactional
    public void removeById(String sId) {
        E entity = entityManager.find(clsEntity, sId);
        this.remove(entity);
    }

    /**
     * Gibt alle Entities innerhalb der Datenbank zurück.
     *
     * @return Liste aller Entities
     * @see List
     */
    public List<E> findAll() {
        List<E> resultList = entityManager.createQuery("select e from " + clsEntity.getSimpleName() + " AS e", clsEntity).getResultList();
        return resultList;
    }

    /**
     * Gibt auf eine bestimmte Menge begrenzte Anzahl Entities innerhalb der Datenbank zurück.
     *
     * @param limit Anzahl auf die die Ergebnisse begrenzt werden sollen
     * @return Menge der Entites als Liste
     * @see List
     */
    public List<E> findAll(Integer limit) {
        if (limit == null)
            return findAll();

        List<E> resultList = entityManager.createQuery("select e from " + clsEntity.getSimpleName() + " AS e", clsEntity).setMaxResults(limit).getResultList();
        return resultList;
    }
}
