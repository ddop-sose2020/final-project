package de.hallinto.core.model.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import java.util.Set;

@Entity
public class Mitarbeiter extends Person {

    @OneToMany(fetch = FetchType.EAGER)
    public Set<Rolle> rollen;

    private boolean aktivangestellt;
    private String username;
    private String password;

    /**
     * @return
     */
    public boolean isAktivangestellt() {
        return aktivangestellt;
    }

    /**
     * @param aktivangestellt
     */
    public void setAktivangestellt(boolean aktivangestellt) {
        this.aktivangestellt = aktivangestellt;
    }

    /**
     * @return Rollen eines Mitarbeiters
     */
    public Set<Rolle> getRollen() {
        return rollen;
    }

    /**
     * @param rollen
     */
    public void setRollen(Set<Rolle> rollen) {
        this.rollen = rollen;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
