package de.hallinto.core.model.dao;

import de.hallinto.core.model.entity.Patient;

import javax.persistence.TypedQuery;
import java.time.Instant;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Data Access Object (DAO) zur vereinfachten Handhabung der Entity Patient.
 *
 * @see Patient
 * @see de.hallinto.core.model.dao.GenericDAO
 */

public class PatientDAO extends GenericDAO<Patient> {


    private boolean isSet(String param) {
        return param != null && !param.equals("");
    }

    /**
     * @param limit Anzahl auf die die Ergebnisse begrenzt werden sollen
     * @return Menge der Patienten als Liste
     */
    @Override
    public List<Patient> findAll(Integer limit) {
        return super.findAll(limit);
    }

    public List<Patient> findByFilter(String sName, String sVorname, String sBDay) {
        /*
        // Check for valid filters (= isSet)
        if (!isSet(sName) && !isSet(sVorname) && !isSet(sBDay))
            return null;

        String sQuery = "select e from " + clsEntity.getSimpleName() + " AS e WHERE";

        if (isSet(sName))
            sQuery += " lower(e.nachname) LIKE :name";

        if (isSet(sName) && isSet(sVorname))
            sQuery += " AND";

        if (isSet(sVorname))
            sQuery += " lower(e.vorname) LIKE :vorname";

        if ((isSet(sName) || isSet(sVorname)) && isSet(sBDay))
            sQuery += " AND";

        if (isSet(sBDay))
            sQuery += " e.geburtsdatum = :bday";

        TypedQuery<Patient> typedQuery = entityManager.createQuery(sQuery, clsEntity);

        if (isSet(sName))
            typedQuery.setParameter("name", sName.toLowerCase() + "%");

        if (isSet(sVorname))
            typedQuery.setParameter("vorname", sVorname.toLowerCase() + "%");

        if (isSet(sBDay))
            typedQuery.setParameter("bday", Date.from(Instant.ofEpochSecond(Long.valueOf(sBDay))));

        List<Patient> resultList = typedQuery.getResultList();
        */

        LinkedList<Patient> resultList = new LinkedList<>();

        List<Patient> allPats = findAll();

        boolean searchForName = isSet(sName);
        boolean searchForVorname = isSet(sVorname);
        boolean searchByBDay = isSet(sBDay);
        Date bday = searchByBDay ? new Date(Long.valueOf(sBDay)) : null;

        for (Patient pat : allPats) {
            String lowerPatNachname = pat.getNachname() != null ? pat.getNachname().toLowerCase() : "";
            String lowerPatVorname = pat.getVorname() != null ? pat.getVorname().toLowerCase() : "";

            boolean doPush = false;

            if(searchForName)
                if(lowerPatNachname.startsWith(sName.toLowerCase()))
                    doPush = true;
                else
                    doPush = false;

            if(searchForVorname)
                if(lowerPatVorname.startsWith(sVorname.toLowerCase()))
                    doPush = true;
                else
                    doPush = false;

            if(searchByBDay)
                if(pat.getGeburtsdatum() != null && pat.getGeburtsdatum().compareTo(bday) == 0)
                    doPush = true;
                else
                    doPush = false;

            if(doPush)
                resultList.push(pat);
        }


        return resultList;
    }
}
