package de.hallinto.core.model.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import java.util.Date;

@Entity
public class Belegung extends HEntity {
    protected Date datum;
    protected Date sollUhrzeitVon;
    protected Date sollUhrzeitBis;

    protected Date istUhrzeitVon;
    protected Date istUhrzeitBis;

    private String token;

    private Boolean currentAktive;
    private Boolean finished;

    @OneToOne(fetch = FetchType.EAGER)
    private Patient pat;

    @ManyToOne(fetch = FetchType.EAGER)
    private Raum raum;


    /**
     * @return Datum der Belegung
     */
    public Date getDatum() {
        return datum;
    }

    /**
     * @param datum
     */
    public void setDatum(Date datum) {
        this.datum = datum;
    }

    /**
     * @return Soll-Von-Uhrzeit der Belegung
     */
    public Date getSollUhrzeitVon() {
        return sollUhrzeitVon;
    }

    /**
     * @param sollUhrzeitVon
     */
    public void setSollUhrzeitVon(Date sollUhrzeitVon) {
        this.sollUhrzeitVon = sollUhrzeitVon;
    }

    /**
     * @return Soll-Bis-Uhrzeit der Belegung
     */
    public Date getSollUhrzeitBis() {
        return sollUhrzeitBis;
    }

    /**
     * @param sollUhrzeitBis
     */
    public void setSollUhrzeitBis(Date sollUhrzeitBis) {
        this.sollUhrzeitBis = sollUhrzeitBis;
    }

    /**
     * @return Ist-Von-Uhrzeit der Belegung
     */
    public Date getIstUhrzeitVon() {
        return istUhrzeitVon;
    }

    /**
     * @param istUhrzeitVon
     */
    public void setIstUhrzeitVon(Date istUhrzeitVon) {
        this.istUhrzeitVon = istUhrzeitVon;
    }

    /**
     * @return Ist-Bis-Uhrzeit der Belegung
     */
    public Date getIstUhrzeitBis() {
        return istUhrzeitBis;
    }

    /**
     * @param istUhrzeitBis
     */
    public void setIstUhrzeitBis(Date istUhrzeitBis) {
        this.istUhrzeitBis = istUhrzeitBis;
    }

    /**
     * @return Token der Belegung
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token
     */
    public void setToken(String token) {
        this.token = token;
    }

    public Patient getPat() {
        return pat;
    }

    public void setPat(Patient pat) {
        this.pat = pat;
    }

    public Raum getRaum() {
        return raum;
    }

    public void setRaum(Raum raum) {
        this.raum = raum;
    }

    public Boolean getCurrentAktive() {
        return currentAktive == null ? false : currentAktive;
    }

    public void setCurrentAktive(Boolean currentAktive) {
        this.currentAktive = currentAktive;
    }

    public Boolean getFinished() {
        return finished == null ? false : finished;
    }

    public void setFinished(Boolean finished) {
        this.finished = finished;
    }
}
