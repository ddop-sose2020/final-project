package de.hallinto.core.model.dao;

import de.hallinto.core.model.entity.Raum;

/**
 * Data Access Object (DAO) zur vereinfachten Handhabung der Entity Rolle.
 *
 * @see Raum
 * @see GenericDAO
 */

public class RaumDAO extends GenericDAO<Raum> {


}
