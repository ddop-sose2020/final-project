package de.hallinto.core;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/hallinto/v1")
public class Hallinto extends Application {

}