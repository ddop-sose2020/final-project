package de.hallinto.core.rest.api;

import de.hallinto.core.model.entity.Belegung;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaJAXRSCXFCDIServerCodegen", date = "2018-04-26T21:20:51.095+02:00")
public interface BelegungApiService {
    public Response addBelegung(Belegung belegung, SecurityContext securityContext);

    public Response deleteBelegung(String id, SecurityContext securityContext);

    public Response findBelegung(Integer limit, SecurityContext securityContext);

    public Response findBelegungById(String id, SecurityContext securityContext);
}
