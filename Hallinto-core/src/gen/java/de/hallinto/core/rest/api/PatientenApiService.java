package de.hallinto.core.rest.api;

import de.hallinto.core.model.entity.Patient;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaJAXRSCXFCDIServerCodegen", date = "2018-04-26T21:20:51.095+02:00")
public interface PatientenApiService {
    public Response addPatient(Patient patient, SecurityContext securityContext);

    public Response deletePatient(String id, SecurityContext securityContext);

    public Response findPatientById(String id, SecurityContext securityContext);

    public Response findPatienten(Integer limit, SecurityContext securityContext);
}
