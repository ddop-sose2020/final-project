package de.hallinto.core.rest.api;

import de.hallinto.core.ErrorResponse;
import de.hallinto.core.model.entity.Rolle;
import io.swagger.annotations.*;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@Path("/rollen")
@RequestScoped

@Api(description = "the rollen API")
@Consumes({"application/json"})
@Produces({"application/json"})
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaJAXRSCXFCDIServerCodegen", date = "2018-04-26T21:20:51.095+02:00")

public class RollenApi {

    @Context
    SecurityContext securityContext;

    @Inject
    RollenApiService delegate;


    @POST

    @Consumes({"application/json"})
    @Produces({"application/json"})
    @ApiOperation(value = "", notes = "Erstellt eine neue Rolle mit gegebenen Parametern", response = Rolle.class, tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Erfolgreiche Erstellung mit erstellter Rolle als Antwort", response = Rolle.class),
            @ApiResponse(code = 200, message = "Unerwarteter Fehler", response = ErrorResponse.class)})
    public Response addRolle(@ApiParam(value = "Rolle welche persistiert werden soll", required = true) Rolle rolle) {
        return delegate.addRolle(rolle, securityContext);
    }

    @DELETE
    @Path("/{id}")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    @ApiOperation(value = "", notes = "Löscht eine Rolle mit gegebener ID", response = Void.class, tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Erfolgreiche Löschung", response = Void.class),
            @ApiResponse(code = 200, message = "Unerwarteter Fehler", response = ErrorResponse.class)})
    public Response deleteRolle(@ApiParam(value = "ID der Rolle welche gelöscht werden soll", required = true) @PathParam("id") String id) {
        return delegate.deleteRolle(id, securityContext);
    }

    @GET
    @Path("/{id}")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    @ApiOperation(value = "", notes = "Sucht nach einer einzelnen Rolle mit gegebener ID", response = Rolle.class, tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Rolle aus Datenbank welche gegebener ID entspricht", response = Rolle.class),
            @ApiResponse(code = 200, message = "Unerwarteter Fehler", response = ErrorResponse.class)})
    public Response findRolleById(@ApiParam(value = "ID der Rolle nach der gesucht werden soll", required = true) @PathParam("id") String id) {
        return delegate.findRolleById(id, securityContext);
    }

    @GET

    @Consumes({"application/json"})
    @Produces({"application/json"})
    @ApiOperation(value = "", notes = "Gibt alle Rollen innerhalb der Datenbank zurück", response = Rolle.class, responseContainer = "List", tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Erfolgreiche Antwort mit Array aller Rollen", response = Rolle.class, responseContainer = "List"),
            @ApiResponse(code = 200, message = "Unerwarteter Fehler", response = ErrorResponse.class)})
    public Response findRollen(@ApiParam(value = "Maximale Anzahl von Ergebnissen") @QueryParam("limit") Integer limit) {
        return delegate.findRollen(limit, securityContext);
    }
}
