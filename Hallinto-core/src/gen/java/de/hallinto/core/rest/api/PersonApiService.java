package de.hallinto.core.rest.api;

import de.hallinto.core.model.entity.Person;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaJAXRSCXFCDIServerCodegen", date = "2018-04-26T21:20:51.095+02:00")
public interface PersonApiService {
    public Response addPerson(Person person, SecurityContext securityContext);

    public Response deletePerson(String id, SecurityContext securityContext);

    public Response findPerson(Integer limit, SecurityContext securityContext);

    public Response findPersonById(String id, SecurityContext securityContext);
}
