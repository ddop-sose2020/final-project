package de.hallinto.core.rest.api;

import de.hallinto.core.model.entity.Raum;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

public interface RaumApiService {
    public Response addRaum(Raum raum, SecurityContext securityContext);

    public Response deleteRaum(String id, SecurityContext securityContext);

    public Response findRaumById(String id, SecurityContext securityContext);

    public Response findAllRaum(Integer limit, SecurityContext securityContext);
}
