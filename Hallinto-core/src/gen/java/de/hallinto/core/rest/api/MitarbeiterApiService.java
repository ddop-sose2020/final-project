package de.hallinto.core.rest.api;

import de.hallinto.core.model.entity.Mitarbeiter;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@RequestScoped
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaJAXRSCXFCDIServerCodegen", date = "2018-04-26T21:20:51.095+02:00")
public interface MitarbeiterApiService {
    public Response addMitarbeiter(Mitarbeiter mitarbeiter, SecurityContext securityContext);

    public Response deleteMitarbeiter(String id, SecurityContext securityContext);

    public Response findMitarbeiter(Integer limit, SecurityContext securityContext);

    public Response findMitarbeiterById(String id, SecurityContext securityContext);

    public Boolean login(String username, String password);
}
