package de.hallinto.core.rest.api;

import de.hallinto.core.ErrorResponse;
import de.hallinto.core.model.entity.Patient;
import io.swagger.annotations.*;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@Path("/patienten")
@RequestScoped

@Api(description = "the patienten API")
@Consumes({"application/json"})
@Produces({"application/json"})
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaJAXRSCXFCDIServerCodegen", date = "2018-04-26T21:20:51.095+02:00")

public class PatientenApi {

    @Context
    SecurityContext securityContext;

    @Inject
    PatientenApiService delegate;


    @POST

    @Consumes({"application/json"})
    @Produces({"application/json"})
    @ApiOperation(value = "", notes = "Erstellt einen neuen Patienten in der Datenbank", response = Patient.class, tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Erfolgreiche Erstellung mit neuem Patienten als Antwort", response = Patient.class),
            @ApiResponse(code = 200, message = "Unerwarteter Fehler", response = ErrorResponse.class)})
    public Response addPatient(@ApiParam(value = "Patient der in der Datenbank gespeichert werden soll", required = true) Patient patient) {
        return delegate.addPatient(patient, securityContext);
    }

    @DELETE
    @Path("/{id}")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    @ApiOperation(value = "", notes = "Löscht einen Patienten mit gegebener ID aus der Datenbank", response = Void.class, tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Erfolgreiche Löschung", response = Void.class),
            @ApiResponse(code = 200, message = "Unerwarteter Fehler", response = ErrorResponse.class)})
    public Response deletePatient(@ApiParam(value = "ID des Patient welcher gelöscht werden soll", required = true) @PathParam("id") String id) {
        return delegate.deletePatient(id, securityContext);
    }

    @GET
    @Path("/{id}")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    @ApiOperation(value = "", notes = "Gibt einen Patienten basierend auf einer gegebenen ID zurück", response = Patient.class, tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Erfolgreiche Ausgabe des Patienten welcher gegebener ID entspricht", response = Patient.class),
            @ApiResponse(code = 200, message = "Unerwarteter Fehler", response = ErrorResponse.class)})
    public Response findPatientById(@ApiParam(value = "ID des Patienten welcher zurückgegeben werden soll", required = true) @PathParam("id") String id) {
        return delegate.findPatientById(id, securityContext);
    }

    @GET

    @Consumes({"application/json"})
    @Produces({"application/json"})
    @ApiOperation(value = "", notes = "Gibt alle Patienten innerhalb der Datenbank zurück", response = Patient.class, responseContainer = "List", tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Erfolgreiche Ausgabe aller Patienten als Array", response = Patient.class, responseContainer = "List"),
            @ApiResponse(code = 200, message = "Unerwarteter Fehler", response = ErrorResponse.class)})
    public Response findPatienten(@ApiParam(value = "Maximale Anzahl der Ergebnisse") @QueryParam("limit") Integer limit) {
        return delegate.findPatienten(limit, securityContext);
    }
}
