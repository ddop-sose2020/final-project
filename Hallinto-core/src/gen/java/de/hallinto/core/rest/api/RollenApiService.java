package de.hallinto.core.rest.api;

import de.hallinto.core.model.entity.Rolle;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaJAXRSCXFCDIServerCodegen", date = "2018-04-26T21:20:51.095+02:00")
public interface RollenApiService {
    public Response addRolle(Rolle rolle, SecurityContext securityContext);

    public Response deleteRolle(String id, SecurityContext securityContext);

    public Response findRolleById(String id, SecurityContext securityContext);

    public Response findRollen(Integer limit, SecurityContext securityContext);
}
