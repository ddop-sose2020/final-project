package de.hallinto.core.rest.api;

import de.hallinto.core.ErrorResponse;
import de.hallinto.core.model.entity.Person;
import io.swagger.annotations.*;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@Path("/person")
@RequestScoped

@Api(description = "the person API")
@Consumes({"application/json"})
@Produces({"application/json"})
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaJAXRSCXFCDIServerCodegen", date = "2018-04-26T21:20:51.095+02:00")

public class PersonApi {

    @Context
    SecurityContext securityContext;

    @Inject
    PersonApiService delegate;


    @POST

    @Consumes({"application/json"})
    @Produces({"application/json"})
    @ApiOperation(value = "", notes = "Erstellt eine neue Person in der Datenbank", response = Person.class, tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Erfolgreich erstellte Person", response = Person.class),
            @ApiResponse(code = 200, message = "Unerwarteter Fehler", response = ErrorResponse.class)})
    public Response addPerson(@ApiParam(value = "Person die zur Datenbank hinzugefügt werden soll", required = true) Person person) {
        return delegate.addPerson(person, securityContext);
    }

    @DELETE
    @Path("/{id}")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    @ApiOperation(value = "", notes = "Löscht eine Person basierend auf seiner ID", response = Void.class, tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Person erfolgreich gelöscht", response = Void.class),
            @ApiResponse(code = 200, message = "Unerwarteter Fehler", response = ErrorResponse.class)})
    public Response deletePerson(@ApiParam(value = "ID der Person", required = true) @PathParam("id") String id) {
        return delegate.deletePerson(id, securityContext);
    }

    @GET

    @Consumes({"application/json"})
    @Produces({"application/json"})
    @ApiOperation(value = "", notes = "Gibt alle Personen innerhalb der Datenbank zurück", response = Person.class, responseContainer = "List", tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Sammlung der Personen als Array", response = Person.class, responseContainer = "List"),
            @ApiResponse(code = 200, message = "Unerwarteter Fehler", response = ErrorResponse.class)})
    public Response findPerson(@ApiParam(value = "Maximale Anzahl der Ergebnisse") @QueryParam("limit") Integer limit) {
        return delegate.findPerson(limit, securityContext);
    }

    @GET
    @Path("/{id}")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    @ApiOperation(value = "", notes = "Gibt einen einzelnen Patienten basierend auf seiner ID zurück", response = Person.class, tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Person aus Datenbank welche gegebener ID entspricht", response = Person.class),
            @ApiResponse(code = 200, message = "Unerwarteter Fehler", response = ErrorResponse.class)})
    public Response findPersonById(@ApiParam(value = "ID der Person nach der gesucht werden soll", required = true) @PathParam("id") String id) {
        return delegate.findPersonById(id, securityContext);
    }
}
