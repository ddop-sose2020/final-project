package de.hallinto.core.rest.api;

import de.hallinto.core.ErrorResponse;
import de.hallinto.core.model.entity.Mitarbeiter;
import io.swagger.annotations.*;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@Path("/mitarbeiter")
@RequestScoped

@Api(description = "the mitarbeiter API")
@Consumes({"application/json"})
@Produces({"application/json"})
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaJAXRSCXFCDIServerCodegen", date = "2018-04-26T21:20:51.095+02:00")

public class MitarbeiterApi {

    @Context
    SecurityContext securityContext;

    @Inject
    MitarbeiterApiService delegate;


    @POST

    @Consumes({"application/json"})
    @Produces({"application/json"})
    @ApiOperation(value = "", notes = "Erstellt einen neuen Mitarbeiter in der Datenbank", response = Mitarbeiter.class, tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Erfolgreich erstellter Mitarbeiter", response = Mitarbeiter.class),
            @ApiResponse(code = 200, message = "Unerwarteter Fehler", response = ErrorResponse.class)})
    public Response addMitarbeiter(@ApiParam(value = "Mitarbeiter der erstellt werden soll", required = true) Mitarbeiter mitarbeiter) {
        return delegate.addMitarbeiter(mitarbeiter, securityContext);
    }

    @DELETE
    @Path("/{id}")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    @ApiOperation(value = "", notes = "Löscht einen Mitarbeiter basierend auf seiner ID aus der Datenbank", response = Void.class, tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Mitarbeiter erfolgreich gelöscht", response = Void.class),
            @ApiResponse(code = 200, message = "Unerwarteter Fehler", response = ErrorResponse.class)})
    public Response deleteMitarbeiter(@ApiParam(value = "ID des Mitarbeiters", required = true) @PathParam("id") String id) {
        return delegate.deleteMitarbeiter(id, securityContext);
    }

    @GET

    @Consumes({"application/json"})
    @Produces({"application/json"})
    @ApiOperation(value = "", notes = "Gibt alle Mitarbeiter in der Datenbank zurück", response = Mitarbeiter.class, responseContainer = "List", tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Alle Patienten aus der Datenbank als Array", response = Mitarbeiter.class, responseContainer = "List"),
            @ApiResponse(code = 200, message = "Unerwarteter Fehler", response = ErrorResponse.class)})
    public Response findMitarbeiter(@ApiParam(value = "Maximale Anzahl der Ergebnisse") @QueryParam("limit") Integer limit) {
        return delegate.findMitarbeiter(limit, securityContext);
    }

    @GET
    @Path("/{id}")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    @ApiOperation(value = "", notes = "Gibt den der ID entsprechenden Patienten zurück", response = Mitarbeiter.class, tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Mitarbeiter aus Datenbank welche gegebener ID entspricht", response = Mitarbeiter.class),
            @ApiResponse(code = 200, message = "Unerwarteter Fehler", response = ErrorResponse.class)})
    public Response findMitarbeiterById(@ApiParam(value = "ID nach der gesucht werden soll", required = true) @PathParam("id") String id) {
        return delegate.findMitarbeiterById(id, securityContext);
    }

}
