# Hallinto-core

## Abhänigkeiten
### Kompilieren
* JDK 8
* Maven

### Ausführen (Container)
* Docker
* Docker-Compose

### Ausführen (Nativ)
* WildFly 12 JavaEE Application Server

## Kompilieren & Ausführen

```
./buildAndRun.sh
```

## Kompilieren
```
mvn clean package
```

## Ausführen
Nachdem das Maven-Projekt erfolgreich kompiliert ist, kann es ausgeführt werden.

Durch das .war-Packaging und die JavaEE-8 Kompatiblität sollte Hallinto auf jeden JavaEE-8 Application Server ausführbar sein.

Entwickelt und getestet ist es jedoch ausschließlich mit dem WildFly 12 Application Server.

Die einfachste Möglichkeit um Hallinto auszuführen, ist mithilfe von Docker.

### Docker Container
Docker und Docker-Compose sind Voraussetzungen, um Hallinto auf die einfachste Art zu starten.

Zuvor muss ein Docker-Image erstellt werden, dem das kompilierte .war-Package und die Swagger-Konfiguration beigefügt ist sowie eine WildFly-Instanz beinhaltet.

```
docker build -t de.hallinto.core/hallinto-core .
```

Danach kann man Hallinto via dem enthaltenen Shell-Skript bedienen:

* Container erstellen & starten:
`./hallinto-core.sh create`

* Container stoppen:
`./hallinto-core.sh stop`

* Container starten:
`./hallinto-core.sh start`

* Container löschen:
`./hallinto-core.sh destroy`

* Logs einsehen:
`./hallinto-core.sh logs`

### Ausführen (Nativ)
Alternativ kann das .war-Package auf eine bevorzugte Weiße an einen Application Server depoyed werden.

```
cp ./target/hallinto-core.war ${WILDFLY_DEPLOY_DIR}
```

Weiter ist zu beachten, dass die Datenbank (MongoDB) über den Hostname "mongodb" angesprochen wird.

Es muss entsprechende eine MongoDB-Instanz laufen und Host-Einträge (mongodb -> MongoDB-Server-IP) vorgenommen werden.
(Entfällt bei der Docker-Variante)

## Frontend
Um mit dem Backend kommunizieren und es benutzen zu können wird das Frontend aus dem Repository

```
hallinto-ui-ionic
```

gebraucht.

Frontend und Backend müssen nicht zwangsweiße auf dem selben Host laufen.

**In jedem Fall ist im Frontend eine Überprüfung und  - wenn nötig - eine Anpassung der API-URL (Backend) nötig!**