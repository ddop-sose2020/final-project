import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {RegistryProvider} from "../registry/registry";

/*
  Generated class for the BelegungProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class BelegungProvider {

  apiUrl;

  constructor(public http: HttpClient, reg: RegistryProvider) {
    console.log('Hello BelegungProvider Provider');
    this.apiUrl = reg.getApiUrl();
  }

  getAllBelegungen() {
    return new Promise(resolve => {
      this.http.get(this.apiUrl+'/belegung').subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

  getById(id) {
    return new Promise(resolve => {
      this.http.get(this.apiUrl+'/belegung/'+id).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

  getByRaumIdAndStartEnd(raumId, start, end) {
    return new Promise((resolve, reject) => {
      this.http.get(this.apiUrl+'/srvBelegung/getByRaumIdAndStartEnd?rID='+raumId+"&s="+start+"&e="+end).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
        reject(err);
      });
    });
  }

  getByToken(token) {
    return new Promise((resolve, reject) => {
      this.http.get(this.apiUrl+'/srvBelegung/getByToken/'+token).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
        reject(err);
      });
    });
  }

  save(dat) {
    return new Promise((resolve, reject) => {
      this.http.post(this.apiUrl + '/belegung', dat).subscribe(data => {
        //...
        resolve(data);
      }, err => {
        console.log(err);
        reject(err);
      });
    });
  }

  delete(id) {
    return new Promise((resolve, reject) => {
      this.http.delete(this.apiUrl + '/belegung/'+id).subscribe(data => {
        //...
        resolve(data);
      }, err => {
        console.log(err);
        reject(err);
      });
    });
  }

}
