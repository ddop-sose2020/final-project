import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the RegistryProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RegistryProvider {

  private apiUrl = 'http://backend.ddop-sen-sose20.code-et-more.net/hallinto/v1';

  constructor(public http: HttpClient) {
    console.log('Hello RegistryProvider Provider');
  }


  getApiUrl() {
    return this.apiUrl;
  }

}
