import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MitarbeiterCreatePage } from './mitarbeiter-create';

@NgModule({
  declarations: [
    MitarbeiterCreatePage,
  ],
  imports: [
    IonicPageModule.forChild(MitarbeiterCreatePage),
  ],
})
export class MitarbeiterCreatePageModule {}
