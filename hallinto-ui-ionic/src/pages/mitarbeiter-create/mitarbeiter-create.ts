import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MitarbeiterProvider} from "../../providers/mitarbeiter/mitarbeiter";

/**
 * Generated class for the MitarbeiterCreatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mitarbeiter-create',
  templateUrl: 'mitarbeiter-create.html',
})
export class MitarbeiterCreatePage {

  mitForm: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams, private formBuilder: FormBuilder, public mitarbeiter: MitarbeiterProvider, public toastCtrl: ToastController) {
    this.mitForm = this.formBuilder.group({
      vorname: ['', Validators.required],
      nachname: ['', Validators.required],
      username: ['', Validators.required],
      password: ['', Validators.required],
      rollen: [false]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MitarbeiterCreatePage');
  }

  createMit() {
    let dataToSave = this.mitForm.value;
    if(dataToSave.rollen == true) {
      dataToSave.rollen = [{
        id: "5b049d61857aba0037075d81"
      }];
    } else {
      dataToSave.rollen = [];
    }

    this.mitarbeiter.save(dataToSave).then((data: any) => {
      if(data != null && data.id != null) {
        let toast = this.toastCtrl.create({
          message: 'Mitarbeiter erfolgreich angelegt!',
          duration: 3000
        });
        toast.present().then(() => {
          this.navCtrl.popToRoot();
        });
      } else {
        let toast = this.toastCtrl.create({
          message: 'Mitarbeiter anlegen gescheitert!',
          duration: 3000
        });
        toast.present();
      }
    }).catch((err) => {
      let toast = this.toastCtrl.create({
        message: 'Mitarbeiter anlegen gescheitert!',
        duration: 3000
      });
      toast.present();
    });;
  }

}
