import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BelegungEinsehenPage } from './belegung-einsehen';

@NgModule({
  declarations: [
    BelegungEinsehenPage,
  ],
  imports: [
    IonicPageModule.forChild(BelegungEinsehenPage),
  ],
})
export class BelegungEinsehenPageModule {}
