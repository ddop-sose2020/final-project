import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BelegungDetailPage } from './belegung-detail';

@NgModule({
  declarations: [
    BelegungDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(BelegungDetailPage),
  ],
})
export class BelegungDetailPageModule {}
