import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PatCreatePage } from './pat-create';

@NgModule({
  declarations: [
    PatCreatePage,
  ],
  imports: [
    IonicPageModule.forChild(PatCreatePage),
  ],
})
export class PatCreatePageModule {}
