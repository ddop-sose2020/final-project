import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {RaumProvider} from "../../providers/raum/raum";

/**
 * Generated class for the RaumVwPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-raum-vw',
  templateUrl: 'raum-vw.html',
})
export class RaumVwPage {

  allRooms = null;

  constructor(public navCtrl: NavController, public navParams: NavParams, public raum: RaumProvider) {
    this.allRooms = navParams.get("raumlst");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RaumVwPage');
  }

  addRaum() {
    this.navCtrl.push('RaumCreatePage', {});
  }

  openDetail(id) {
    this.raum.getById(id).then(data => {
      this.navCtrl.push('RaumDetailPage', {raumData: data});
    });
  }

}
