import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RaumVwPage } from './raum-vw';

@NgModule({
  declarations: [
    RaumVwPage,
  ],
  imports: [
    IonicPageModule.forChild(RaumVwPage),
  ],
})
export class RaumVwPageModule {}
