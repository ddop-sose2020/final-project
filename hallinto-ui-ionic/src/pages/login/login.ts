import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, AlertController, ToastController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { MitarbeiterProvider } from '../../providers/mitarbeiter/mitarbeiter';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  credentialsForm: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams, menu: MenuController, private formBuilder: FormBuilder, public alertCtrl: AlertController, public mitarbeiter: MitarbeiterProvider, public toastCtrl: ToastController) {
    // disable main menu for login page
    menu.enable(false);

    // instatiate login form
    this.credentialsForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  login() {
    console.log("login...");
  }

  onSignIn() {


    this.mitarbeiter.login(this.credentialsForm.value).then((data) => {
      console.dir(data);
      if(data == true) {
        this.navCtrl.setRoot('DashboardPage', { currentUser: this.credentialsForm.value.username});
      } else {
        let toast = this.toastCtrl.create({
          message: 'Anmeldung gescheitert!',
          duration: 3000
        });
        toast.present();
      }
    }).catch((data) => {
      let toast = this.toastCtrl.create({
        message: 'Anmeldung gescheitert!',
        duration: 3000
      });
      toast.present();
    });
  }

  onForgotPassword() {
    let alert = this.alertCtrl.create({
      title: 'Passwort vergessen',
      subTitle: 'Bitte wenden Sie sich an Ihren Systemadministrator.',
      buttons: ['OK']
    });
    alert.present();
  }

}
