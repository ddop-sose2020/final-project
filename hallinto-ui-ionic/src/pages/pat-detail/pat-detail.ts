import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {PatientProvider} from "../../providers/patient/patient";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

/**
 * Generated class for the PatDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pat-detail',
  templateUrl: 'pat-detail.html',
})
export class PatDetailPage {

  pat: any = {
    id: "",
    vorname: "",
    nachname: "",
    geschlecht: "",
    geburtsdatum: "",
    kostentraeger: "",
    krankenkasse: "",
    versicherungsnummer: "",
    versicherungsstatus: ""
  };

  patForm: FormGroup;

  patShort = "";

  dateToString(input) {
    let date = new Date(input);
    let day = date.getDate();
    let month = date.getMonth() + 1;

    if(day < 10)

    return (day < 10 ? "0" : "") + day + "." + (month < 10 ? "0" : "") + month + "." + date.getFullYear();
  }

  constructor(public navCtrl: NavController, public navParams: NavParams, public patient: PatientProvider, private formBuilder: FormBuilder, public toastCtrl: ToastController, public alertCtrl: AlertController) {
    let patData = navParams.get("patData");

    // Patient Mockup ...
    //this.pat = JSON.parse("{\"vorname\":\"Max\",\"nachname\":\"Mustermann\",\"geschlecht\":\"M\",\"geburtsdatum\":\"01.01.2000\",\"kostentraeger\":\"Kasse\",\"krankenkasse\":\"AOK\",\"versicherungsnummer\":\"1234567890\",\"versicherungsstatus\":null,\"id\":\"5afcbff6433ac893b92103b6\",\"defaultValue\":null}");

    //this.patient.getById(patId).then(pat => {
    //   this.pat = pat;
    //});

    console.dir(patData);

    this.pat = patData;

    this.patForm = this.formBuilder.group({
      id: [this.pat.id],
      vorname: [this.pat.vorname],
      nachname: [this.pat.nachname],
      geschlecht: [this.pat.geschlecht],
      geburtsdatum: [new Date(this.pat.geburtsdatum).toISOString()],
      kostentraeger: [this.pat.kostentraeger],
      krankenkasse: [this.pat.krankenkasse],
      versicherungsnummer: [this.pat.versicherungsnummer],
      versicherungsstatus: [this.pat.versicherungsstatus]
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PatDetailPage');
  }

  savePat() {
    this.patient.save(this.patForm.value).then((data: any) => {
      console.dir(data);
      if(data != null && data.id != null) {
        this.pat = this.patient.addOwnToString(data);
        let toast = this.toastCtrl.create({
          message: 'Speichern erfolgreich!',
          duration: 3000
        });
        toast.present();
      } else {
        let toast = this.toastCtrl.create({
          message: 'Speichern gescheitert!',
          duration: 3000
        });
        toast.present();
      }
    });
  }

  deletePat() {
    let alert = this.alertCtrl.create({
      title: 'Datensatz löschen',
      subTitle: 'Patient "' + this.pat.nachname + ', ' + this.pat.vorname + ' (*' + this.pat.geburtsdatum + ')" wirklich löschen?',
      buttons: [
        {
          text: 'Ja, löschen',
          handler: () => {
            this.patient.delete(this.pat.id).then(data => {
              let toast = this.toastCtrl.create({
                message: 'Datensatz gelöscht.',
                duration: 1500
              });
              toast.present().then(() => {
                this.navCtrl.popToRoot();
              });
            });
          }
        },
        {
          text: 'Nein',
          handler: () => {
            console.log('Do not delete');
          }
        }
      ]
    });
    alert.present();
  }

}
