import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PatSuchePage } from './pat-suche';

@NgModule({
  declarations: [
    PatSuchePage,
  ],
  imports: [
    IonicPageModule.forChild(PatSuchePage),
  ],
})
export class PatSuchePageModule {}
