import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MitarbeiterProvider} from "../../providers/mitarbeiter/mitarbeiter";
import {RaumProvider} from "../../providers/raum/raum";

/**
 * Generated class for the RaumCreatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-raum-create',
  templateUrl: 'raum-create.html',
})
export class RaumCreatePage {

  raumForm: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams, private formBuilder: FormBuilder, public raum: RaumProvider, public toastCtrl: ToastController) {
    this.raumForm = this.formBuilder.group({
      bezeichnung: ['', Validators.required],
      nummer: ['', Validators.required]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RaumCreatePage');
  }

  createRaum() {
    let dataToSave = this.raumForm.value;

    this.raum.save(dataToSave).then((data: any) => {
      if(data != null && data.id != null) {
        let toast = this.toastCtrl.create({
          message: 'Raum erfolgreich angelegt!',
          duration: 3000
        });
        toast.present().then(() => {
          this.navCtrl.popToRoot();
        });
      } else {
        let toast = this.toastCtrl.create({
          message: 'Raum anlegen gescheitert!',
          duration: 3000
        });
        toast.present();
      }
    }).catch((err) => {
      let toast = this.toastCtrl.create({
        message: 'Raum anlegen gescheitert!',
        duration: 3000
      });
      toast.present();
    });
  }

}
