import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {RaumProvider} from "../../providers/raum/raum";

/**
 * Generated class for the RaumDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-raum-detail',
  templateUrl: 'raum-detail.html',
})
export class RaumDetailPage {

  raumData: any = {
    id: "",
    bezeichnung: "",
    nummer: "",
  };

  raumForm: FormGroup;


  constructor(public navCtrl: NavController, public navParams: NavParams, private formBuilder: FormBuilder, public raum: RaumProvider, public toastCtrl: ToastController, public alertCtrl: AlertController) {

    this.raumData = navParams.get("raumData");

    this.raumForm = this.formBuilder.group({
      id: [this.raumData.id],
      bezeichnung: [this.raumData.bezeichnung, Validators.required],
      nummer: [this.raumData.nummer, Validators.required]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RaumDetailPage');
  }

  saveRaum() {
    let dataToSave = this.raumForm.value;

    this.raum.save(dataToSave).then((data: any) => {
      console.dir(data);
      if(data != null && data.id != null) {
        this.raumData = data;
        let toast = this.toastCtrl.create({
          message: 'Speichern erfolgreich!',
          duration: 3000
        });
        toast.present().then(() => {
          this.navCtrl.popToRoot();
        });
      } else {
        let toast = this.toastCtrl.create({
          message: 'Speichern gescheitert!',
          duration: 3000
        });
        toast.present();
      }
    }).catch((err) => {
      let toast = this.toastCtrl.create({
        message: 'Speichern gescheitert!',
        duration: 3000
      });
      toast.present();
    });
  }

  deleteRaum() {
    let alert = this.alertCtrl.create({
      title: 'Datensatz löschen',
      subTitle: 'Raum "' + this.raumData.bezeichnung + ' [' + this.raumData.nummer + ']" wirklich löschen?',
      buttons: [
        {
          text: 'Ja, löschen',
          handler: () => {
            this.raum.delete(this.raumData.id).then(data => {
              let toast = this.toastCtrl.create({
                message: 'Datensatz gelöscht.',
                duration: 1500
              });
              toast.present().then(() => {
                this.navCtrl.popToRoot();
              });
            });
          }
        },
        {
          text: 'Nein',
          handler: () => {
            console.log('Do not delete');
          }
        }
      ]
    });
    alert.present();
  }

}
