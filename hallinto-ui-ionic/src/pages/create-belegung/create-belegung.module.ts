import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateBelegungPage } from './create-belegung';

@NgModule({
  declarations: [
    CreateBelegungPage,
  ],
  imports: [
    IonicPageModule.forChild(CreateBelegungPage),
  ],
})
export class CreateBelegungPageModule {}
