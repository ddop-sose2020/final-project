import { Component } from '@angular/core';
import {AlertController, IonicPage, ModalController, NavController, NavParams, ToastController} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {BelegungProvider} from "../../providers/belegung/belegung";

/**
 * Generated class for the CreateBelegungPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-create-belegung',
  templateUrl: 'create-belegung.html',
})
export class CreateBelegungPage {

  public roomName;
  public belegungTime;

  public room;

  public belegungForm: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams, private formBuilder: FormBuilder, public modalCtrl: ModalController, public belegung: BelegungProvider, public toastCtrl: ToastController, public alert: AlertController) {
    console.dir(this.navParams);

    this.belegungForm = this.formBuilder.group({
      datum: ["", Validators.required],
      sollUhrzeitVon: ["", Validators.required],
      sollUhrzeitBis: ["", Validators.required],
      pat: ["", Validators.required]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreateBelegungPage');

    try {
      this.room = this.navParams.get("room");

      this.roomName = this.room.bezeichnung + " [" + this.room.nummer + "]";

      let start = this.navParams.get("start");
      let end = this.navParams.get("end");

      this.belegungTime = start.format("DD.MM.YYYY") + " " + start.format("HH:mm") + ' Uhr - ' + end.format("HH:mm") + ' Uhr';

      this.belegungForm.setValue({
        datum: start.format(),
        sollUhrzeitVon: start.format(),
        sollUhrzeitBis: end.format(),
        pat: ""
      });
    } catch (e) {
      console.error(e);
    }
  }

  createBelegung() {
    let dataToSave = this.belegungForm.value;

    // set room
    dataToSave['raum'] = this.room;

    console.dir(dataToSave);

    this.belegung.save(dataToSave).then((data: any) => {
      if(data != null && data.id != null) {
        let alert = this.alert.create({
          title: "Belegung gebucht",
          subTitle: 'Token: '+data.token,
          buttons: [
            {
              text: 'Schließen',
              handler: () => {
                this.navCtrl.pop();
              }
            }
          ]
        });
        alert.present();
      } else {
        let toast = this.toastCtrl.create({
          message: 'Belegung anlegen gescheitert!',
          duration: 3000
        });
        toast.present();
      }
    }).catch((err) => {
      let toast = this.toastCtrl.create({
        message: 'Belegung anlegen gescheitert!',
        duration: 3000
      });
      toast.present();
    });
  }

  searchPat() {
    /*
    let searchModal = this.modalCtrl.create('PatSuchePage', { modal: true });
    searchModal.onDidDismiss(data => {
      console.log(data);
    });
    searchModal.present();
    */

    new Promise((resolve, reject) => {
      this.navCtrl.push('PatSuchePage', { modal: resolve });
    }).then((pat: any) => {
      // now fill in pat field in form



      this.belegungForm.controls['pat'].setValue(pat);
    });


  }

}
