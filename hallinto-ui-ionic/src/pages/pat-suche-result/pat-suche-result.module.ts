import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PatSucheResultPage } from './pat-suche-result';

@NgModule({
  declarations: [
    PatSucheResultPage,
  ],
  imports: [
    IonicPageModule.forChild(PatSucheResultPage),
  ],
})
export class PatSucheResultPageModule {}
