import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {MitarbeiterProvider} from "../../providers/mitarbeiter/mitarbeiter";

/**
 * Generated class for the MitarbeiterVwPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mitarbeiter-vw',
  templateUrl: 'mitarbeiter-vw.html',
})
export class MitarbeiterVwPage {

  allMitarbeiter = null;

  constructor(public navCtrl: NavController, public navParams: NavParams, public mitarbeiter: MitarbeiterProvider) {
    this.allMitarbeiter = navParams.get("mitarbeiterlst");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MitarbeiterVwPage');
  }

  addMitarbeiter() {
    this.navCtrl.push('MitarbeiterCreatePage', {});
  }

  openDetail(id) {
    this.mitarbeiter.getById(id).then(data => {
      this.navCtrl.push('MitarbeiterDetailPage', {mitData: data});
    });
  }

}
