import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MitarbeiterVwPage } from './mitarbeiter-vw';

@NgModule({
  declarations: [
    MitarbeiterVwPage,
  ],
  imports: [
    IonicPageModule.forChild(MitarbeiterVwPage),
  ],
})
export class MitarbeiterVwPageModule {}
