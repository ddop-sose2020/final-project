import { Component } from '@angular/core';
import {IonicPage, MenuController, NavController, NavParams} from 'ionic-angular';
import {MitarbeiterProvider} from "../../providers/mitarbeiter/mitarbeiter";
import {RaumProvider} from "../../providers/raum/raum";

/**
 * Generated class for the DashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {

  public currentUser;

  public allRooms;

  constructor(public navCtrl: NavController, public navParams: NavParams, public menu: MenuController, public mitarbeiter: MitarbeiterProvider, public raum: RaumProvider) {
    menu.enable(true);

    this.currentUser = mitarbeiter.getAuthenticatedUsername();

    if(this.currentUser === false) {
      navCtrl.setRoot("LoginPage");
    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DashboardPage');

    this.raum.getAllRaeume().then((lstRooms) => {
      this.allRooms = lstRooms;
    });
  }

  roomSelected(selectedRoomId) {
    this.navCtrl.push('RaumPlanPage', {room: selectedRoomId});
  }

}
